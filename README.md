# Angular App Setup

This document provides a basic setup guide for an Angular application, including instructions for Dockerfile setup and GitLab CI configuration.

## Dockerfile

The Dockerfile provided below is used to containerize the Angular application:

```Dockerfile
# Use the official Node.js image as the base
FROM node:latest

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy project files into the container
COPY . .

# Install dependencies
RUN npm install

# Build the application
RUN npm run build

# Expose the port on which the application will be accessible
EXPOSE 4200

# Command to start the application when the container is launched
CMD ["npm", "start"]
```

Explanation:

- FROM node:latest: Uses the latest official Node.js image as the base image.
- WORKDIR /usr/src/app: Sets the working directory in the container.
- COPY . .: Copies the project files into the container.
- RUN npm install: Installs project dependencies.
- RUN npm run build: Builds the Angular application.
- EXPOSE 4200: Exposes port 4200, on which the application will be accessible.
- CMD ["npm", "start"]: Specifies the command to start the application when the container is launched.


## Gitlab CI

The GitLab CI configuration provided below automates testing and building processes:

```yaml
stages:
  - test
  - build

test:
  stage: test
  image: node:latest
  script:
    - npm install
    - npm run lint

build:
  stage: build
  image: docker:19.03  # Use an image with Docker-in-Docker
  services:
    - docker:19.03-dind  # Use Docker-in-Docker service
  script:
    - docker build -t angular .
    - docker run -d -p 80:4200 angular
```

- stages: Defines the stages of the CI pipeline.
- test: Runs linting tests on the Angular project.
- build: Builds and deploys the Angular application using Docker.
- image: node:latest: Specifies the Node.js image for the testing stage.
- image: docker:19.03: Specifies the Docker-in-Docker image for the build stage.
- services: Enables Docker-in-Docker service.
- docker build -t angular .: Builds a Docker image named "angular" from the current directory.
- docker run -d -p 80:4200 angular: Runs the Docker container in detached mode, mapping port 80 on the host to port 4200 in the container.
