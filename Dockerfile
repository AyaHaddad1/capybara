# Utiliser l'image officielle Node.js en tant que base
FROM node:latest

# Définir le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copier les fichiers du projet dans le conteneur
COPY . .

# Installer les dépendances
RUN npm install

# Construire l'application
RUN npm run build

# Exposer le port sur lequel l'application sera accessible
EXPOSE 4200

# Commande pour démarrer l'application lorsque le conteneur est lancé
CMD ["npm", "start"]
